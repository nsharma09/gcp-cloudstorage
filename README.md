## Instructions
* Install the python package manager
    * apt-get update 
    * apt-get install python3-pip
* Install the storage library
    * pip3 install google-cloud-storage
* Get the code
    * git clone https://gitlab.com/synechron-gcp-batch1/gcp-cloudstorage
    * cd gcp-cloud-storage
* Just update the bucket name in the file
    * vi create_bucket.py
* Execute the code using below command
    * python3 create_bucket.py
